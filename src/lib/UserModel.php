<?php
namespace Phppot\src\lib;

use Couchbase\PrefixSearchQuery;

class UserModel
{

    private $conn;

    function __construct()
    {
        require_once 'DataSource.php';
        $this->conn = new DataSource();

        if (isset($_POST['method']) && $_POST['method'] === 'getAllUser' ) {
            $this->getAllUser($_POST['value'],$_POST['order']);
        }
        if (isset( $_FILES['file']) && !empty($_FILES['file'])) {
            $this->readUserRecords();
        }

    }

    /**
     * @param string $filter
     * @param string $order
     * @return array
     */
    function getAllUser($filter = 'id', $order = 'asc')
    {
        $sqlSelect = "SELECT * FROM users ORDER BY $filter $order";

        $result = $this->conn->select($sqlSelect);

        if (isset($_POST['method']) && $_POST['method'] === 'getAllUser' ) {
           echo json_encode($result);
        }

        return $result;
    }

    /**
     * @param null $data
     * @return mixed
     */
    function readUserRecords($data = null)
    {
        $fileCsv = $_FILES['file'];

        $fileName = $fileCsv["tmp_name"];
        if ($fileCsv["size"] > 0) {
            $file = fopen($fileName, "r");

            while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                if (! empty($column) && is_array($column)) {
                    if ($this->hasEmptyRow($column)) {
                        continue;
                    }
                    $category = $column[1];
                    $firstname = $column[2];
                    $lastname = $column[3];
                    $email = $column[4];
                    $gender = $column[5];
                    $birthDate = $column[6];
                    $insertId = $this->insertUser($category, $firstname, $lastname, $email, $gender, $birthDate);
                    if (! empty($insertId)) {
                        $output["type"] = "success";
                        $output["message"] = "Import completed.";
                    }
                } else {
                    $output["type"] = "error";
                    $output["message"] = "Problem in importing data.";
                }
            }
            echo json_encode($output);
            return $output;
        }
    }

    /**
     * @param array $column
     * @return bool
     */
    function hasEmptyRow(array $column)
    {
        $columnCount = count($column);
        $isEmpty = true;
        for ($i = 0; $i < $columnCount; $i ++) {
            if (! empty($column[$i]) || $column[$i] !== '') {
                $isEmpty = false;
            }
        }
        return $isEmpty;
    }

    /**
     * @param $category
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $gender
     * @param $birthDate
     * @return int
     */
    function insertUser($category, $firstname, $lastname, $email, $gender, $birthDate)
    {
        $sql = "SELECT firstname FROM users WHERE firstname = ?";
        $paramType = "s";
        $paramArray = array(
            $category
        );
        $result = $this->conn->select($sql, $paramType, $paramArray);
        $insertId = 0;
        if (empty($result)) {
            $sql = "INSERT into users (category, firstname, lastname, email, gender, birthDate)
                       values (?,?,?,?,?,?)";
            $paramType = "ssssss";
            $paramArray = array(
                    $category, $firstname, $lastname, $email, $gender, $birthDate
            );
            $insertId = $this->conn->insert($sql, $paramType, $paramArray);
        }
        return $insertId;
    }
}

new UserModel();
?>
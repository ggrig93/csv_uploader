<?php

use Phppot\src\lib\UserModel;

$userModel = new UserModel();
    $result = $userModel->getAllUser(); ?>
     <div class="table-div">
<?php if (!empty($result)) {
    ?>
    <h3>Imported records:</h3>

    <table id='userTable'>
        <thead>
        <tr>
            <th data-name="id">Id</th>
            <th data-name="category">Category</th>
            <th data-name="firstname">First Name</th>
            <th data-name="lastname">Last Name</th>
            <th data-name="email">Email</th>
            <th data-name="gender">Gender</th>
            <th data-name="birthDate">BirthDate</th>
        </tr>
        </thead>

        <tbody class="csv-data">
        <?php
        foreach ($result as $row) { ?>
        <tr>
            <td class="csv-id"><?php echo $row['id']; ?></td>
            <td class="csv-category"><?php echo $row['category']; ?></td>
            <td class="csv-firstname"><?php echo $row['firstname']; ?></td>
            <td class="csv-lastname"><?php echo $row['lastname']; ?></td>
            <td class="csv-email"><?php echo $row['email']; ?></td>
            <td class="csv-gender"><?php echo $row['gender']; ?></td>
            <td class="csv-birthDate"><?php echo $row['birthDate']; ?></td>
        </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

    <?php
}
?>
     </div>

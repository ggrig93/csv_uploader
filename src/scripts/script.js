// function validateFile() {
//     let csvInputFile = document.forms["frmCSVImport"]["file"].value;
//     if (csvInputFile == "") {
//         documemt.getElementById("response").innerHTML = "No source found to import";
//         return false;
//     }
//     return true;
// }
const success = 'success'

$(document).ready(function () {
    $(document).on('click', 'th', function () {
        $(this).toggleClass('desc')
        let value = $(this).attr('data-name')
        let order
        if ($(this).hasClass('desc')) {
            order = 'desc'
        } else {
            order = 'asc'
        }
        $.ajax({
            url: "lib/UserModel.php",
            data: {value: value, order: order, method: 'getAllUser'},
            method: "POST",
            success: function (res) {
                $('.csv-data').empty()
                JSON.parse(res).forEach((item) => {
                    $('.csv-data').append(
                        `<tr><td> ${item.id} </td>
                    <td> ${item.category} </td>
                    <td> ${item.firstname} </td>
                    <td> ${item.lastname}  </td>
                    <td> ${item.email} </td>
                    <td> ${item.gender} </td>
                    <td> ${item.birthDate} </td></tr>`
                    )
                })
            }
        });

    })

    $('#submitCsv').click(function () {
        let fileData = $('#fileCsv').prop('files')[0];
        if (!fileData) {
            $('#response').html("No source found to import")
            return false;
        }
        let formData = new FormData();
        formData.append('file', fileData);
        $.ajax({
            url: "lib/UserModel.php",
            method: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            // data: {formData, method: 'readUserRecords'},
            success: function (res) {
                res = JSON.parse(res)
                if (res.type == 'success') {
                    $('#fileCsv').val('')
                }
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        })
            .then(function () {
                $.ajax({
                    url: "lib/UserModel.php",
                    data: {value: 'id', order: 'asc', method: 'getAllUser'},
                    method: "POST",
                    success: function (res) {
                        if (!$('#userTable').length) {
                            $('.table-div').append(`
                            <table id='userTable'>
                                 <thead>
        <tr>
            <th data-name="id">Id</th>
            <th data-name="category">Category</th>
            <th data-name="firstname">First Name</th>
            <th data-name="lastname">Last Name</th>
            <th data-name="email">Email</th>
            <th data-name="gender">Gender</th>
            <th data-name="birthDate">BirthDate</th>
        </tr>
        </thead>

        <tbody class="csv-data">
   
        </tbody>
    </table>  `)

                        }
                        $('.csv-data').empty()
                        JSON.parse(res).forEach((item) => {
                            $('.csv-data').append(
                                `<tr>
                                    <td> ${item.id} </td>
                                    <td> ${item.category} </td>
                                    <td> ${item.firstname} </td>
                                    <td> ${item.lastname}  </td>
                                    <td> ${item.email} </td>
                                    <td> ${item.gender} </td>
                                    <td> ${item.birthDate} </td>
                                </tr>`
                            )
                        })

                    }
                });
            })
    })
});
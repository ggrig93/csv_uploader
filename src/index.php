<?php
namespace Phppot;

use Phppot\src\lib\UserModel;

require_once __DIR__ . '/lib/UserModel.php';
?>
<html>
<head>
    <link href="./style/style.css" rel="stylesheet" type="text/css"/>
    <script src="./scripts/jquery-3.2.1.min.js"></script>
    <script src="./scripts/script.js"></script>
</head>
<body>
<h2>Import CSV file into Mysql using PHP</h2>
<div class="outer-scontainer">
    <div class="row">

        <div Class="input-row">
            <input type="file" name="fileCsv" id="fileCsv"
                   class="file" accept=".csv,.xls,.xlsx">
            <div class="import">
                <button type="submit" id="submitCsv" name="import" class="btn-submit">Import</button>
            </div>
        </div>

    </div>

    <div id="response"
         class="<?php if (!empty($response["type"])) {
             echo $response["type"];
         } ?>">
        <?php if (!empty($response["message"])) {
            echo $response["message"];
        } ?>
    </div><?php require_once __DIR__ . '/list.php'; ?></div>
</body>
</html>